"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 18:04
@Author  : 派大星
@Site    :
@File    : send_wx_notice.py
@Software: PyCharm
@desc:
"""
import requests


class Robot:
    """企业微信机器人"""

    def __init__(self):
        self.url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=XXXXX'

    def markdown_robot(self, build_url, fail_case_title):
        """markdown消息文档"""
        data_json = {
            "msgtype": "markdown",
            "markdown": {
                "content": f"【OneSig】构建<font color=\"warning\">失败！</font>\n"
                           f">测试报告链接:<font color=\"info\"> [{build_url}]({build_url})</font>\n"
                           f">失败用例名称:<font color=\"info\"> {fail_case_title}</font>\n"
            }
        }
        try:
            requests.post(url=self.url, json=data_json)
        except Exception as err:
            raise err


if __name__ == '__main__':
    Robot().markdown_robot('https://www.baidu.com', 'test')

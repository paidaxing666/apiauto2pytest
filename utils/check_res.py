"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 17:21
@Author  : 派大星
@Site    :
@File    : check_res.py
@Software: PyCharm
@desc:
"""
import pytest_check as check
import requests.models


def check_status_res_code(res: requests.models.Response, description: str = '', res_code: int = 0) -> dict:
    """
    校验HTTP响应状态码
    :param res: HTTP响应对象
    :param description: 函数描述
    :param res_code: 业务code
    :return: json对象
    """
    _status_code = res.status_code
    if _status_code != 200:
        check.equal(_status_code, 200, f'{description}返回状态码为{_status_code}')
    else:
        res_dict = res.json()
        if 'status_code' in res_dict:
            response_code = res_dict.get('status_code')
        else:
            response_code = res_dict.get('code')
        check.equal(response_code, res_code, f'{description}{res_dict}')
        return res_dict

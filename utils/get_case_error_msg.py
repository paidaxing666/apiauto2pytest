"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 18:03
@Author  : 派大星
@Site    :
@File    : get_case_error_msg.py
@Software: PyCharm
@desc:
"""
import os
import re
from pathlib import Path


BASE_DIR = Path(__file__).resolve().parent.parent  # 获取项目路径


class GetCaseErrorMsg:
    """解析jenkins.log，获取失败用例标题"""
    def __init__(self):
        self.file = os.path.join(BASE_DIR, 'logs', 'jenkins.log')

    def get_fail_case_detail(self):
        """
        获取失败用例主要信息
        优先取报错接口返回msg，若msg为空，则取用例函数路径
        """
        _fail_msg = []
        _fail_dir = []
        fail_case_line = []

        # 定义不同错误类型的正则表达式
        failure_regex = r'FAILURE:.*'
        timeout_regex = r'E\s+Failed:\s+Timeout\s*>.*s'
        type_regex = r'E\s+TypeError:\s+.*'
        failed_regex = r'FAILED\s+.*'
        error_regex = r'^ERROR\s+.*'
        json_error_regex = r'E\s+json\.decoder\.JSONDecodeError'
        base_regex = r'E\s+[\w.]+:'

        with open(self.file, 'r', encoding='utf-8') as f:
            for line in f:
                line = line.strip()

                failure_line = re.findall(failure_regex, line)
                timeout_line = re.findall(timeout_regex, line)
                type_line = re.findall(type_regex, line)
                failed_line = re.findall(failed_regex, line)
                error_line = re.findall(error_regex, line)
                json_error_line = re.findall(json_error_regex, line)
                base_line = re.findall(base_regex, line)

                if failure_line:  # 取FAILURE开头的报错log
                    line_str = line.replace('FAILURE:', '').strip()
                    _fail_msg.append(line_str)
                elif json_error_line:  # 处理接口204时，提示信息
                    _fail_msg.append('')
                elif timeout_line:  # 取接口超时的报错log
                    _fail_msg.append('')
                elif type_line:  # 取接口报错的报错log
                    _fail_msg.append('')
                elif base_line:
                    _fail_msg.append('')
                elif failed_line or error_line:  # 取FAILED或ERROR开头的报错log
                    line_dir_str = re.split(r'::', line)[1]  # 取FAILED开头的报错信息：接口对应函数名
                    _fail_dir.append(line_dir_str)
            for index, msg in enumerate(_fail_msg):  # 遍历FAILURE
                if msg == '':  # if is null,说明未对接口做报错后的提示，则把报错接口的对应函数名存入列表
                    if len(_fail_dir) > index:  # 单个用例会出现多个判断出错，此时在判断结果中反取用例名，
                        # 可能出现列表越界，该情况不再取值，也不在提醒该用例的错误，属于通知失败用例不全面的范畴
                        fail_case_line.append(_fail_dir[index])
                    else:
                        pass
                else:
                    fail_case_line.append(msg)  # 把msg存入列表
        if len(fail_case_line) > 0:
            fail_case_title = '\n'.join(fail_case_line)
            print(fail_case_title)
            return fail_case_title


if __name__ == '__main__':
    GetCaseErrorMsg().get_fail_case_detail()

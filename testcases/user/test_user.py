"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 17:20
@Author  : 派大星
@Site    :
@File    : test_user.py
@Software: PyCharm
@desc:
"""
from utils.check_res import check_status_res_code


def test_admin_user_info(user_api):
    """用户信息"""
    res = user_api.admin_user_info()

    # 断言
    check_status_res_code(res, test_admin_user_info.__doc__)

"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 17:17
@Author  : 派大星
@Site    :
@File    : conftest.py
@Software: PyCharm
@desc:
"""
import pytest
from api.user import User

# 定义全局变量，用于储存键值对
global_data = {}


@pytest.fixture
def set_global_data():
    """设置全局变量"""

    def _set_global_data(key, value):
        global_data[key] = value

    return _set_global_data


@pytest.fixture
def get_global_data():
    """获取全局变量值"""

    def _get_global_data(key):
        return global_data.get(key, None)

    return _get_global_data


@pytest.fixture(scope='session')
def user_api(base_url):
    """用户实例化"""
    user_api = User(base_url)
    return user_api

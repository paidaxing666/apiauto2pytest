"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 10:43
@Author  : 派大星
@Site    :
@File    : run.py.py
@Software: PyCharm
@desc:
"""
import sys
from utils.get_case_error_msg import GetCaseErrorMsg
from utils.send_wx_notice import Robot


def run():
    """主函数"""
    fail_case_title = GetCaseErrorMsg().get_fail_case_detail()  # 获取失败用例标题
    if fail_case_title:
        build_url = sys.argv[1]
        print(build_url)
        Robot().markdown_robot(build_url=build_url, fail_case_title=fail_case_title)
    else:
        print('测试通过！')


if __name__ == '__main__':
    run()

"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 17:29
@Author  : 派大星
@Site    :
@File    : conftest.py
@Software: PyCharm
@desc:
"""
from datetime import datetime
import pytest


def pytest_html_report_title(report):
    """自定义报告标题"""
    report.title = "ApiAuto-接口测试报告!"


def pytest_html_results_table_header(cells):
    """新增报告列"""
    cells.insert(3, "<th>Description</th>")
    cells.insert(4, '<th class="sortable time" data-column-type="time">Time</th>')
    cells.pop()


def pytest_html_results_table_row(report, cells):
    """插入报告列"""
    cells.insert(3, f"<td>{report.description}</td>")
    cells.insert(4, f'<td class="col-time">{datetime.now()}</td>')
    cells.pop()


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """新增用例描述列"""
    outcome = yield
    report = outcome.get_result()

    if item.function.__doc__ is None:
        report.description = str(item.function.__name__)
    else:
        report.description = str(item.function.__doc__)
    # report.nodeid = report.nodeid.encode("unicode_escape").decode("utf-8")

# ApiAuto2pytest简介

## 依赖
- Python >=3.9+
- Redis >=6.0+
- 基础库
  - requests
  - jsonpath
- pytest相关插件
  - pytest：pytest框架
  - pytest-html：结果报告
  - pytest-base-url：基础URL-目的是用于URL抽离，快速切换测试正式环境（实际项目中未利用）
  - pytest-check：断言-允许每个函数进行多次失败检查
  - pytest-variables：命令行指定文件，读取文件成字典，提供给测试用例
  - pytest-timeout：用例计时，超时停止用例
  - pytest-rerunfailures：重新运行测试用例以消除间歇性故障

## 安装使用
1.安装依赖库
```python
pip install -r requirements.txt
```
2.在命令行中执行测试用例
```python
pytest
```
3.查看测试报告
```text
在命令行或reports中查看测试报告
```

## 待开发
- [X] 失败用例收集
- [X] 接口失败推送到企业微信群
- [X] 接口测试报告自定义
- [X] 用例失败重试
- [ ] 用例用例分级调用
- [ ] 用例调用频次控制
- [ ] 接口监控Jenkins部署
- [ ] 接口用例快速生成
- [ ] 接口Redis缓存信息
- [ ] XXX

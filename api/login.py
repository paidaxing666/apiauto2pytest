"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 14:37
@Author  : 派大星
@Site    : 
@File    : login.py
@Software: PyCharm
@desc:
"""
import requests
import logging


class Login:
    """登录获取用户cookie，然后赋值session，供全局接口请求使用"""

    def __init__(self, base_url):
        self.base_url = base_url
        self.session = requests.session()
        self.session.headers = self.get_header()

    def get(self, url, **kwargs):
        """封装GET请求"""
        logging.info(f'发送 GET 请求 {url} {kwargs}')
        if not url.startswith('http'):
            if url.startswith('/'):
                url = self.base_url + url
            else:
                url = self.base_url + '/' + url
        res = self.session.get(url, **kwargs)
        logging.info(f'响应结果为 {res.text}')
        return res

    def post(self, url, **kwargs):
        """封装POST请求"""
        logging.info(f'发送 POST 请求 {url} {kwargs}')
        if not url.startswith('http'):
            if url.startswith('/'):
                url = self.base_url + url
            else:
                url = self.base_url + '/' + url
        res = self.session.post(url, **kwargs)
        logging.info(f'响应结果为 {res.text}')
        return res

    def login(self):
        """登录获取access_token"""
        url = '/auth/oauth/token'
        headers = {
            "Authorization": "Basic cGlnOnBpZw=="
        }
        params = {
            "randomStr": "blockPuzzle",
            "grant_type": "password"
        }
        json_data = {
            "username": "gabcs11",
            "password": "PyJgT72W"
        }
        res = self.post(url, headers=headers, params=params, data=json_data)
        if res.status_code == 200:
            access_token = res.json().get('access_token')
            return access_token

    def get_header(self):
        """获取用户登录状态的headers"""
        headers = {"Authorization": f"Bearer {self.login()}"}
        return headers


if __name__ == '__main__':
    res11 = Login('http:/xxxxx:8070').login()
    print(res11.text)

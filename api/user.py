"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/9/22 17:10
@Author  : 派大星
@Site    :
@File    : user.py
@Software: PyCharm
@desc:
"""
from api.login import Login


class User(Login):
    """用户接口"""

    def admin_user_info(self):
        """用户信息"""
        url = '/admin/user/info'
        res = self.get(url)
        return res
